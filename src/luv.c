#define LUA_LIB
#include "luv.h"

LUALIB_API int luaopen_uv(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        /*ENTRY(run),*/
#undef  ENTRY
        { NULL, NULL }
    };
    luaL_newlib(L, libs);
    return 1;
}
