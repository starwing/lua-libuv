#ifndef luv_h
#define luv_h


#include <lua.h>
#include <lauxlib.h>

#include "../libuv/include/uv.h"
#include "ngx-queue.h"


#define LUV_TYPES(XX) \
    XX(STATE,   State,      "state")   \
  /*XX(STREAM,  Stream,     "stream")*/\
    XX(FIBER,   Fiber,      "fiber")   \
    XX(QUEUE,   Queue,      "queue")   \

#define LUV_STREAM_TYPES(XX) \
    XX(TCP,     tcp,        "net.tcp") \
    XX(TTY,     tty,        "tty")     \
    XX(NAMED_PIPE, pipe,    "pipe")    \

#define LUV_UV_TYPES(XX) \
    LUV_STREAM_TYPES(XX) \
    XX(UDP,     udp,        "net.udp") \
    XX(TIMER,   timer,      "timer")   \
    XX(IDLE,    idle,       "idle")    \
    XX(FS_EVENT,fs_event,   "fs.event")\
    XX(FS_POLL, fs_poll,    "fs.poll") \
    XX(FILE,    file,       "file")    \
    XX(PROCESS, process,    "process") \

#define container_of(ptr, type, member) \
  ((type*) ((char*)(ptr) - offsetof(type, member)))


/* interfaces */

LUALIB_API int luaopen_uv         (lua_State *L);
LUALIB_API int luaopen_uv_errno   (lua_State *L);
LUALIB_API int luaopen_uv_fiber   (lua_State *L);
LUALIB_API int luaopen_uv_queue   (lua_State *L);
LUALIB_API int luaopen_uv_pipe    (lua_State *L);
LUALIB_API int luaopen_uv_timer   (lua_State *L);
LUALIB_API int luaopen_uv_idle    (lua_State *L);
LUALIB_API int luaopen_uv_net_tcp (lua_State *L);
LUALIB_API int luaopen_uv_net_udp (lua_State *L);


/* types */

typedef ngx_queue_t luv_Queue;
typedef struct luv_Fiber luv_Fiber;
typedef struct luv_State luv_State;
typedef struct luv_Stream luv_Stream;

typedef enum luv_Type {
    LUV_TYPE_UNKNOWN,
#define XX(e,t,n) LUV_##e,
    LUV_TYPES(XX)
    LUV_UV_TYPES(XX)
#undef  XX
    LUV_TYPE_NUM
} luv_Type;


/* luv type systems */

const char *luv_typename (luv_Type t);

luv_Type luv_type (lua_State *L, int narg);

void *luv_test  (lua_State *L, int narg, luv_Type t);
void *luv_check (lua_State *L, int narg, luv_Type t);

int luv_check_file (lua_State *L, int narg);

int luv_argferror (lua_State *L, int narg, const char *fmt, ...);
int luv_typeerror (lua_State *L, int narg, const char *tname);
int luv_tostring  (lua_State *L);
int luv_copystack (lua_State *from, lua_State *to, int nargs);
int luv_dumpstack (lua_State *L);

void luv_newmetatable(lua_State *L, luv_Type t, luaL_Reg *libs);
void luv_setmetatable(lua_State *L, luv_Type t);

int luv_sockaddr_info(lua_State *L, struct sockaddr_storage *addr);

#define luv_opt(L, n, t, def) \
    ( lua_isnoneornil((L),(n)) ? \
      (def) : luv_check((L),(n),(t)) )


/* error check */

#define luv_call(L, func, args) do {                \
    int err;                                        \
    if ((err = uv_##func args) != 0)                \
        return luv_push_error(L, #func, err); } while (0)

int luv_push_error (lua_State *L, const char *prefix, int err);


/* luv main state */

#define LUV_STATE_FIELDS \
    /* publuc */       \
    luv_Queue running; \
    luv_Queue waiting; \
    luv_Queue errors;  \
    /* read-only */    \
    int is_ready;      \
    lua_State *L;      \
    uv_loop_t *loop;   \
    uv_async_t async;  \

luv_State *luv_main_state(lua_State *L);
lua_State *luv_main_luastate(lua_State *L);

int luv_state_join  (luv_State *S);
int luv_state_ready (luv_State *S);

#define luv_default_loop(L)  (luv_main_state(L)->loop)
#define luv_state_running(L) (&luv_main_state(L)->running)
#define luv_state_errors(L)  (&luv_main_state(L)->errors)

#define luv_common_yield(f)  luv_fiber_yield((f), &(f)->S->waiting, 0)

#define LUV_IMPLEMENT_COMMON_CB(type) \
static void type##_cb(uv_##type##_t* req, int result) { \
    luv_Fiber *f = container_of(req, luv_Fiber, r);   \
    lua_settop(f->L, 0);                              \
    if (result < 0)                                   \
        luv_push_error(f->L, #type, result);          \
    else                                              \
        lua_pushinteger(f->L, result);                \
    luv_fiber_resume(f, NULL);                        \
}


/* stream interface */

#define LUV_STREAM_FILEDS \
    /* public */          \
    union luv_stream_u h; \
    /* readonly */        \
    uv_buf_t  buf;        \
    union {               \
    luv_Queue listening;  \
    luv_Queue reading;    \
    } q;                  \
    luv_Queue closing;    \
    size_t count;         \

void luv_stream_openlibs (lua_State *L);

luv_Stream *luv_stream_new   (lua_State *L);
void        luv_stream_del   (luv_Stream *s);

luv_Stream *luv_stream_test  (lua_State *L, int narg);
luv_Stream *luv_stream_check (lua_State *L, int narg);

int luv_stream_start (luv_Stream *s);
int luv_stream_stop  (luv_Stream *s);

#define luv_stream_queue(s) (&(s)->head)

#define luv_tcp(s)      (&((s)->h.tcp))
#define luv_tty(s)      (&((s)->h.tty))
#define luv_pipe(s)     (&((s)->h.pipe))
#define luv_stream(s)   (&((s)->h.stream))


/* fiber interface */

#define LUV_FIBER_FILEDS \
    /* public */        \
    union luv_req_u  r; \
    /* read-only */     \
    luv_Queue head;     \
    luv_Queue joined;   \
    lua_State *L;       \
    luv_State *S;       \
    luv_Queue *waiting; \

luv_Fiber *luv_fiber_new (lua_State *L);
void       luv_fiber_del (luv_Fiber *f);

luv_Fiber *luv_fiber_current  (lua_State *L);
luv_Fiber *luv_fiber_default  (lua_State *L, int narg, int *new_narg);

int luv_fiber_retrieve (lua_State *L, luv_Fiber *f);

int luv_fiber_ready  (luv_Fiber *f);
int luv_fiber_join   (luv_Fiber *f, lua_State *L);
int luv_fiber_resume (luv_Fiber *f, lua_State *L);
int luv_fiber_yield  (luv_Fiber *f, luv_Queue *waitat, int nargs);

#define luv_get_fiber(req)  container_of(req, luv_Fiber,  r)

#define luv_fiber_is_mainthread(f) \
    ((f)->L == (f)->S->L)


/* wait queue */

luv_Queue *luv_queue_new (lua_State *L);
void       luv_queue_del (luv_Queue *q);

int  luv_queue_wait    (luv_Queue *q, lua_State *L, int nargs);
void luv_queue_wakeall (luv_Queue *q, lua_State *L);

int luv_queue_pusherrors (lua_State *L, const char *method, luv_Queue *q);

#define luv_queue_init(q)   ngx_queue_init(q)
#define luv_queue_add(q,h)  ngx_queue_add(q,h)
#define luv_queue_empty(q)  ngx_queue_empty(q)
#define luv_queue_head(q)   container_of(ngx_queue_head(q), luv_Fiber, head)

#define luv_queue_wakeup(q,L) \
    luv_fiber_resume(luv_queue_head(q),(L))

#define luv_queue_remove(q) do { \
    ngx_queue_remove(q); \
    ngx_queue_init(q); } while (0)

#define luv_queue_append(q,h) do { \
    ngx_queue_remove(h); \
    ngx_queue_insert_tail(q,h); } while (0)


/* private part */

union luv_stream_u {
#define XX(e, t, n) uv_##t##_t t;
    LUV_STREAM_TYPES(XX)
#undef  XX
    uv_stream_t stream;
};

union luv_req_u {
#define XX(e,t) uv_##t##_t t;
    UV_REQ_TYPE_MAP(XX)
#undef  XX
};

struct luv_Stream { LUV_STREAM_FILEDS };
struct luv_Fiber { LUV_FIBER_FILEDS };
struct luv_State { LUV_STATE_FIELDS };


#endif /* luv_h */
/* vim: set ft=c: */
/* cc: flags+='-s -DLUA_BUILD_AS_DLL -mdll' output='../uv.dll' */
/* cc: input='*.c' libs+='-llua52 ../libuv/libuv.a -lws2_32 -lpsapi -liphlpapi' */
