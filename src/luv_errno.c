#define LUA_LIB
#include "luv.h"


int luv_push_error(lua_State *L, const char *prefix, int err) {
    lua_pushnil(L);
    if (prefix == NULL)
        lua_pushstring(L, uv_strerror(err));
    else
        lua_pushfstring(L, "%s: %s", prefix, uv_strerror(err));
    lua_pushinteger(L, err);
    return 3;
}

static int Lname(lua_State *L) {
    int err = luaL_checkint(L, 1);
    lua_pushstring(L, uv_err_name(err));
    return 1;
}

static int Lmessage(lua_State *L) {
    int err;
    switch (lua_type(L, 1)) {
    case LUA_TSTRING:
        lua_pushvalue(L, 1);
        lua_gettable(L, lua_upvalueindex(1));
        err = lua_tointeger(L, -1);
        lua_pop(L, 1);
        break;
    case LUA_TNUMBER:
        err = lua_tointeger(L, 1);
        break;
    default:
        lua_pushfstring(L, "number/string expected, got %s", luaL_typename(L, 1));
        return luaL_argerror(L, 1, lua_tostring(L, -1));
    }
    lua_pushstring(L, uv_err_name(err));
    return 1;
}

void open_error_map(lua_State *L) {
    struct {
        const char *name;
        uv_errno_t err;
    } errno_array[] = {
#define XX(code,_) { #code, UV_##code },
        UV_ERRNO_MAP(XX)
#undef  XX
    };
    int i, len = sizeof(errno_array)/sizeof(*errno_array);
    lua_newtable(L);
    for (i = 0; i < len; ++i) {
        lua_pushstring(L, errno_array[i].name);
        lua_pushinteger(L, errno_array[i].err);
        lua_rawset(L, -3);
    }
}

LUALIB_API int luaopen_uv_errno(lua_State *L) {
    open_error_map(L);
    lua_pushcfunction(L, Lname);
    lua_setfield(L, -2, "name");
    lua_pushvalue(L, -1);
    lua_pushcclosure(L, Lmessage, 1);
    lua_setfield(L, -2, "message");
    return 1;
}
