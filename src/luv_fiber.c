#define LUA_LIB
#include "luv.h"

#include <assert.h>

/* 
 * Fiber support for lua-libuv
 *
 * a fiber is a userdata combined with a coroutine object. a fiber is
 * always in one of below status:
 *  - initial: f->waitting == NULL
 *             a fiber is free, not ready to resume or wait anything.
 *  - ready: f->waitting == &f->S->running
 *           a fiber is ready to run later.
 *  - waiting: f->waiting != NULL
 *             a fiber is waiting a given queue.
 *  - dead: f->L == NULL
 *          fiber cannot yield or resume any more.
 *
 *  a fiber can used in several ways:
 *  - hold: free a fiber from its waiting, set it to 'hold'
 *  - ready: wakeup a fiber sometime later.
 *  - join: make a fiber wait another fiber's finish.
 *  - resume: wakeup a fiber immediately.
 *  - yield: makes a fiber wait on given queue.
 */


/* fiber status detect */

#define fiber_is_dead(f)  ((f)->L == NULL)
#define fiber_is_hold(f)  ((f)->waiting == NULL)
#define fiber_is_ready(f) ((f)->waiting == &(f)->S->running)

#define fiber_is_waitting(f)  ((f)->waiting != NULL)
#define fiber_is_mainstate(f) ((f)->L == (f)->S->L)


/* fiber registry interface */

#define LUV_FIBER_BOX      ((void*)0xF1BE7B03)

static void new_fiberbox(lua_State *L) {
    int nobox;
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX);
    nobox = lua_isnil(L, -1);
    lua_pop(L, 1);
    if (nobox) {
        lua_createtable(L, 0, 4);
        lua_rawsetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX);
    }
}

static luv_Fiber *new_raw_fiber(lua_State *L) {
    luv_Fiber *f = (luv_Fiber*)lua_newuserdata(L, sizeof(luv_Fiber));
    luv_setmetatable(L, LUV_FIBER);
    f->L = NULL;
    f->S = luv_main_state(L);
    f->waiting = NULL;
    ngx_queue_init(&f->head);
    ngx_queue_init(&f->joined);
    return f;
}

static luv_Fiber *track_fiber(lua_State *L, luv_Fiber *f) {
    /* stack: ud thread */
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX);
    lua_pushvalue(L, -2); /* thread */
    lua_pushvalue(L, -4); /* ud */
    lua_rawset(L, -3); /* ->ptrbox */
    lua_pop(L, 2); /* remove thread, fiber_box */
    return f;
}

static void untrack_fiber(luv_Fiber *f) {
    lua_State *L = f->L;
    assert(L != NULL);
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX);
    lua_pushthread(L);
    lua_pushnil(L);
    lua_rawset(L, -3); /* ->ptrbox */
    lua_pop(L, 1);
}


/* fiber interface */

static luv_Fiber *init_main_fiber(lua_State *L) {
    luv_Fiber *f = new_raw_fiber(L);
    f->L = f->S->L;
    lua_pushthread(f->L);
    if (f->L != L) lua_xmove(f->L, L, 1);
    return track_fiber(L, f);
}

luv_Fiber *luv_fiber_new(lua_State *L) {
    luv_Fiber *f = new_raw_fiber(L);
    f->L = lua_newthread(L);
    return track_fiber(L, f);
}

void luv_fiber_del(luv_Fiber *f) {
    if (!fiber_is_dead(f)) {
        untrack_fiber(f);
        luv_queue_remove(&f->head);
        f->waiting = NULL;
        f->L = NULL;
    }
}

int luv_fiber_retrieve(lua_State *L, luv_Fiber *f) {
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX);
    if (fiber_is_dead(f)) return 0;
    lua_pushthread(f->L);
    lua_xmove(f->L, L, 1);
    lua_rawget(L, -2);
    if (lua_isnil(L, -1)) {
        lua_pop(L, 2);
        return 0;
    }
    lua_remove(L, -2);
    return 1;
}

luv_Fiber *luv_fiber_current(lua_State *L) {
    luv_Fiber *f;
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_FIBER_BOX); /* 1 */
    lua_pushthread(L); /* 2 */
    lua_rawget(L, -2); /* 2->2 */
    if (lua_isnil(L, -1))
        luaL_error(L, "current thread(%p) is not a fiber", L);
    assert(luv_test(L, -1, LUV_FIBER) != NULL);
    f = (luv_Fiber*)lua_touserdata(L, -1);
    if (fiber_is_dead(f))
        luaL_error(L, "current thread(%p) is dead", L);
    lua_pop(L, 2);
    return f;
}

luv_Fiber *luv_fiber_default(lua_State *L, int narg, int *new_narg) {
    luv_Fiber *f;
    if ((f = luv_test(L, narg, LUV_FIBER)) == NULL) {
        if (new_narg) *new_narg = narg;
        return luv_fiber_current(L);
    }
    if (new_narg) *new_narg = narg + 1;
    return f;
}


/* fiber status transfer */

static void process_joined_fiber(luv_Fiber *f, lua_State *L, int res) {
    lua_State *fL = f->L;
    luv_Queue *errors = &f->S->errors;
    assert(res != LUA_YIELD);
    if (res != LUA_OK) {
        ngx_queue_insert_tail(errors, &f->head);
        luv_state_ready(f->S); /* signal a error occurs */
    }
    f->L = NULL; /* marks fiber is dead */
    while (!luv_queue_empty(&f->joined)) {
        luv_Fiber *jf = luv_queue_head(&f->joined);
        assert(!luv_fiber_is_mainthread(f));
        lua_settop(jf->L, 0);
        lua_pushboolean(jf->L, res == LUA_OK);
        luv_copystack(f->L, jf->L, 1);
        luv_fiber_resume(jf, L);
        assert(jf->waiting != &f->joined);
    }
    f->L = fL; /* relive it */
    assert(luv_queue_empty(&f->joined));
}

int luv_fiber_resume(luv_Fiber *f, lua_State *L) {
    int res, nargs;
    if (fiber_is_dead(f))
        return luaL_error(L, "cannot resume dead fiber");
    if (f->waiting) {
        luv_queue_remove(&f->head);
        f->waiting = NULL;
    }
    nargs = lua_gettop(f->L);
    if (lua_status(f->L) == LUA_OK) /* initial state? */
        --nargs;
    assert(ngx_queue_empty(&f->head));
    res = lua_resume(f->L, L, nargs);
    if (res == LUA_OK || res != LUA_YIELD) {
        process_joined_fiber(f, L, res);
        return res;
    }
    return LUA_OK;
}

int luv_fiber_yield(luv_Fiber *f, luv_Queue *waitat, int nargs) {
    if (fiber_is_dead(f))
        return luaL_error(f->L, "attempt to yield a dead fiber");
    if (fiber_is_waitting(f))
        return luaL_error(f->L, "fiber already yielded");
    if (luv_fiber_is_mainthread(f))
        return luaL_error(f->L, "attempt to yield from outside a fiber");
    assert(ngx_queue_empty(&f->head));
    ngx_queue_insert_tail(waitat, &f->head);
    f->waiting = waitat;
    assert(lua_status(f->L) != LUA_YIELD);
    return lua_yield(f->L, nargs);
}

int luv_fiber_join(luv_Fiber *f, lua_State *L) {
    luv_Fiber *cur = luv_fiber_current(L);
    luv_fiber_ready(f);
    if (!fiber_is_mainstate(cur)) { /* join a fiber? */
        return luv_fiber_yield(cur, &f->joined, 0);
    }
    /* else, join to main thread? */
    return luv_state_join(f->S);
}

int luv_fiber_ready(luv_Fiber *f) {
    if (f->waiting == &f->S->running)
        return 0;
    assert(!fiber_is_dead(f) && !fiber_is_waitting(f));
    if (!fiber_is_ready(f)) {
        f->waiting = &f->S->running;
        luv_queue_append(f->waiting, &f->head);
        luv_state_ready(f->S); /* signal a fiber wait run */
    }
    return 0;
}

int luv_fiber_hold(luv_Fiber *f) {
    f->waiting = NULL;
    luv_queue_remove(&f->head);
    return 0;
}


/* fiber runtime routines */

static int Lfiber(lua_State *L) {
    int top = lua_gettop(L);
    luv_Fiber *f;
    luaL_checktype(L, 1, LUA_TFUNCTION);
    f = luv_fiber_new(L);
    lua_insert(L, 1);
    lua_xmove(L, f->L, top);
    luv_fiber_ready(f);
    return 1;
}

static int Lhold(lua_State *L) {
    int narg;
    luv_Fiber *f = luv_fiber_default(L, 1, &narg);
    return luv_fiber_hold(f);
}

static int Lready(lua_State *L) {
    int narg;
    luv_Fiber *f = luv_fiber_default(L, 1, &narg);
    return luv_fiber_ready(f);
}

static int Ljoin(lua_State *L) {
    int nrets;
    luv_Fiber *cur = luv_fiber_current(L);
    luv_Fiber *f = luv_check(L, 1, LUV_FIBER);
    if (!fiber_is_mainstate(cur))
        return luv_fiber_join(f, L);
    /* add errors to join to main thread? */
    nrets = luv_state_join(f->S);
    if (nrets != 0 && !lua_toboolean(L, -nrets))
        lua_error(L);
    return nrets;
}

static int Lyield(lua_State *L) {
    int narg;
    luv_Fiber *f = luv_fiber_default(L, 1, &narg);
    luv_Queue *q = luv_check(L, narg, LUV_QUEUE);
    return luv_fiber_yield(f, q, lua_gettop(L) - narg + 1);
}

static int Lresume(lua_State *L) {
    luv_Fiber *f = luv_check(L, 1, LUV_FIBER);
    lua_xmove(L, f->L, lua_gettop(L) - 1);
    if (luv_fiber_resume(f, L) != LUA_OK) {
        lua_pushnil(L);
        lua_xmove(f->L, L, 1);
        luv_fiber_del(f);
        return 2;
    }
    lua_settop(L, 0);
    lua_pushboolean(L, 1);
    lua_xmove(f->L, L, lua_gettop(f->L));
    return lua_gettop(L);
}

static int Lstatus(lua_State *L) {
    int narg;
    luv_Fiber *f = luv_fiber_default(L, 1, &narg);
    if (f->waiting)
        lua_pushliteral(L, "waiting");
    else if (fiber_is_dead(f))
        lua_pushliteral(L, "dead");
    else switch (lua_status(f->L)) {
    case LUA_YIELD:
        lua_pushliteral(L, "suspended");
        break;
    case LUA_OK: {
         lua_Debug ar;
         if (lua_getstack(f->L, 0, &ar) > 0)  /* does it have frames? */
             lua_pushliteral(L, "normal");  /* it is running */
         else if (lua_gettop(f->L) == 0)
             lua_pushliteral(L, "dead");
         else
             lua_pushliteral(L, "suspended");  /* initial state */
         break;
     }
    default:  /* some error occurred */
        lua_pushliteral(L, "dead");
        break;
    }
    return 1;
}

static int Lrunning(lua_State *L) {
    luv_Fiber *f = luv_fiber_current(L);
    return luv_fiber_retrieve(L, f);
}

static int L__gc(lua_State *L) {
    luv_Fiber *f = luv_test(L, 1, LUV_FIBER);
    if (f) luv_fiber_del(f);
    return 0;
}

LUALIB_API int luaopen_uv_fiber(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(hold),
        ENTRY(ready),
        ENTRY(join),
        ENTRY(yield),
        ENTRY(resume),
        ENTRY(status),
        ENTRY(running),
        ENTRY(__gc),
#undef  ENTRY
        { NULL, NULL }
    };

    new_fiberbox(L);
    luv_newmetatable(L, LUV_FIBER, libs);
    lua_pop(L, 1);
    init_main_fiber(L);
    lua_pushcfunction(L, Lfiber);
    return 1;
}

/* cc: libs+='-llua52' */
