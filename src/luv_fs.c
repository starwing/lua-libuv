#define LUA_LIB
#include "luv.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#ifndef _WIN32
# include <unistd.h>
#endif


/* check functions */

static const char *check_path_or_fd(lua_State *L, int narg, uv_file *fd) {
    if (lua_type(L, narg) == LUA_TNUMBER) {
        *fd = lua_tonumber(L, narg);
        return NULL;
    }
    return luaL_checkstring(L, narg);
}

static double check_time(lua_State *L, int narg) {
    return (double)luaL_checknumber(L, narg);
}

static int check_flags(lua_State *L, int narg) {
    const char *str;
    if (lua_type(L, narg) == LUA_TNUMBER)
        return lua_tonumber(L, narg);
    str = luaL_checkstring(L, narg);
    /* lifted from luvit */
    if (strcmp(str, "r") == 0)
        return O_RDONLY;
    if (strcmp(str, "r+") == 0)
        return O_RDWR;
    if (strcmp(str, "w") == 0)
        return O_CREAT | O_TRUNC | O_WRONLY;
    if (strcmp(str, "w+") == 0)
        return O_CREAT | O_TRUNC | O_RDWR;
    if (strcmp(str, "a") == 0)
        return O_APPEND | O_CREAT | O_WRONLY;
    if (strcmp(str, "a+") == 0)
        return O_APPEND | O_CREAT | O_RDWR;
    return luaL_error(L, "Unknown file open flag: '%s'", str);
}

static int check_mode(lua_State *L, int narg) {
    return strtoul(luaL_checkstring(L, narg), NULL, 8);
}

static void push_stats_table(lua_State* L, struct stat* s) {
    /* lifted from luvit */
    lua_pushinteger(L, s->st_dev);
    lua_setfield(L, -2, "dev");
    lua_pushinteger(L, s->st_ino);
    lua_setfield(L, -2, "ino");
    lua_pushinteger(L, s->st_mode);
    lua_setfield(L, -2, "mode");
    lua_pushinteger(L, s->st_nlink);
    lua_setfield(L, -2, "nlink");
    lua_pushinteger(L, s->st_uid);
    lua_setfield(L, -2, "uid");
    lua_pushinteger(L, s->st_gid);
    lua_setfield(L, -2, "gid");
    lua_pushinteger(L, s->st_rdev);
    lua_setfield(L, -2, "rdev");
    lua_pushinteger(L, s->st_size);
    lua_setfield(L, -2, "size");
#ifdef __POSIX__
    lua_pushinteger(L, s->st_blksize);
    lua_setfield(L, -2, "blksize");
    lua_pushinteger(L, s->st_blocks);
    lua_setfield(L, -2, "blocks");
#endif
    lua_pushinteger(L, s->st_atime);
    lua_setfield(L, -2, "atime");
    lua_pushinteger(L, s->st_mtime);
    lua_setfield(L, -2, "mtime");
    lua_pushinteger(L, s->st_ctime);
    lua_setfield(L, -2, "ctime");
#ifndef _WIN32
    lua_pushboolean(L, S_ISREG(s->st_mode));
    lua_setfield(L, -2, "is_file");
    lua_pushboolean(L, S_ISDIR(s->st_mode));
    lua_setfield(L, -2, "is_directory");
    lua_pushboolean(L, S_ISCHR(s->st_mode));
    lua_setfield(L, -2, "is_character_device");
    lua_pushboolean(L, S_ISBLK(s->st_mode));
    lua_setfield(L, -2, "is_block_device");
    lua_pushboolean(L, S_ISFIFO(s->st_mode));
    lua_setfield(L, -2, "is_fifo");
    lua_pushboolean(L, S_ISLNK(s->st_mode));
    lua_setfield(L, -2, "is_symbolic_link");
    lua_pushboolean(L, S_ISSOCK(s->st_mode));
    lua_setfield(L, -2, "is_socket");
#endif
}

static void push_dir_entry(lua_State *L, const char *names, int count) {
    int i;
    for (i = 0; i < count; ++i) {
        lua_pushstring(L, names);
        lua_rawseti(L, -2, i);
        names += strlen(names) + 1; /* +1 for '\0' */
    }
}

static void luv_fs_result(lua_State *L, uv_fs_t *req) {
    if (req->result != 0) { /* a error occurs */
        lua_settop(L, 0);
        luv_push_error(L, "fs", req->result);
    }
    else switch (req->fs_type) {
    case UV_FS_OPEN:
    case UV_FS_CLOSE:
    case UV_FS_WRITE:
    case UV_FS_SENDFILE:
    case UV_FS_FTRUNCATE:
    case UV_FS_UTIME:
    case UV_FS_FUTIME:
    case UV_FS_CHMOD:
    case UV_FS_FCHMOD:
    case UV_FS_FSYNC:
    case UV_FS_FDATASYNC:
    case UV_FS_UNLINK:
    case UV_FS_RMDIR:
    case UV_FS_MKDIR:
    case UV_FS_RENAME:
    case UV_FS_LINK:
    case UV_FS_SYMLINK:
    case UV_FS_CHOWN:
    case UV_FS_FCHOWN:
        lua_settop(L, 0);
        lua_pushinteger(L, req->result);
        break;

    case UV_FS_READ:
        lua_settop(L, 0);
        lua_pushinteger(L, req->result);
        lua_pushlstring(L, (const char *)req->data, req->result);
        free(req->data);
        req->data = NULL;
        break;

    case UV_FS_READLINK:
        lua_settop(L, 0);
        lua_pushstring(L, (char*)req->ptr);
        break;

    case UV_FS_STAT:
    case UV_FS_LSTAT:
    case UV_FS_FSTAT:
        if (lua_type(L, -1) != LUA_TTABLE)
            lua_newtable(L);
        lua_insert(L, 1);
        lua_settop(L, 1);
        push_stats_table(L, (struct stat*)req->ptr);
        break;

    case UV_FS_READDIR:
        if (lua_type(L, -1) != LUA_TTABLE)
            lua_newtable(L);
        lua_insert(L, 1);
        lua_settop(L, 1);
        push_dir_entry(L, (const char*)req->ptr, req->result);
        break;

    case UV_FS_UNKNOWN:
    case UV_FS_CUSTOM:
        lua_settop(L, 0);
        lua_pushnil(L);
        lua_pushstring(L, "unhandled fs type");
        break;
    }
    uv_fs_req_cleanup(req);
}


/* args & calls macros */

static void fs_cb(uv_fs_t *req) {
    luv_Fiber *f = luv_get_fiber(req);
    luv_fs_result(f->L, req);
    luv_fiber_resume(f, NULL);
}

#define FS_RAWCALL(narg, name, misc, callline) do { \
    int err;                                                \
    luv_Fiber *f    = luv_fiber_default(L, narg, NULL);     \
    uv_fs_cb   cb   = fs_cb;                                \
    uv_loop_t *loop = f->S->loop;                           \
    uv_fs_t   *req  = &f->r.fs;                             \
    if (luv_fiber_is_mainthread(f))                         \
        cb = NULL;                                          \
    req->data = misc;                                       \
    if ((err = (callline)) != 0)                            \
        return luv_push_error(L, #name, err);               \
    if (luv_fiber_is_mainthread(f)) {                       \
        luv_fs_result(L, req);                              \
        return lua_gettop(L);                               \
    }                                                       \
    return luv_fiber_yield(f,                               \
            &f->S->waiting, lua_gettop(f->L));              \
} while (0)

#define FS_CALL(narg, name, misc, ...) \
     FS_RAWCALL(narg, name, misc, \
             uv_fs_##name(loop, req, __VA_ARGS__, cb))

#define FS_CALL2(narg, name, misc, ...) \
    FS_RAWCALL(narg, name, misc,                                   \
            path ? uv_fs_ ##name(loop, req, path, __VA_ARGS__, cb) \
                 : uv_fs_f##name(loop, req, fd,   __VA_ARGS__, cb))

#define PATH(n,name) const char *name = luaL_checkstring(L, n)
#define FD(n,name)   uv_file name = luv_check_file(L, n)
#define ID(n,name)   int name = luaL_checkint(L, n)
#define TIME(n,name) double name = check_time(L, n)
#define SIZE(n,name) size_t name = luaL_checkint(L, n)
#define FLAGS(n) int flags = check_flags(L, n)
#define MODE(n)  int mode = check_mode(L, n)
#define OFFSET(n) int64_t offset = luaL_optint(L, n, 0)
#define BUF(n)  size_t len; \
    const char *buf = luaL_checklstring(L, n, &len)
#define PATH_OR_FD(n) uv_file fd; \
    const char *path = check_path_or_fd(L, n, &fd)


/* fs routines */

static int Lopen(lua_State *L) {
    PATH(1,path); FLAGS(2); MODE(3);
    FS_CALL(4, open, NULL, path, flags, mode);
}

static int Lclose(lua_State *L) {
    FD(1,fd);
    FS_CALL(2, close, NULL, fd);
}

static int Lread(lua_State *L) {
    FD(1,fd); SIZE(2,length); OFFSET(3);
    void *buf = malloc(length);
    if (buf == NULL) {
        lua_pushnil(L);
        lua_pushstring(L, "no memory");
        return 2;
    }
    FS_CALL(4, read, NULL, fd, buf, length, offset);
}

static int Lunlink(lua_State *L) {
    PATH(1,path);
    FS_CALL(2, unlink, NULL, path);
}

static int Lwrite(lua_State *L) {
    FD(1,fd); BUF(2); OFFSET(3);
    FS_CALL(4, write, NULL, fd, (void*)buf, len, offset);
}

static int Lmkdir(lua_State *L) {
    PATH(1,path); MODE(2);
    FS_CALL(3, mkdir, NULL, path, mode);
}

static int Lrmdir(lua_State *L) {
    PATH(1,path);
    FS_CALL(2, rmdir, NULL, path);
}

static int Lreaddir(lua_State *L) {
    PATH(1,path);
    FS_CALL(2, readdir, NULL, path, 0);
}

static int Lstat(lua_State *L) {
    PATH_OR_FD(1);
    FS_RAWCALL(2, stat, NULL,
            path ? uv_fs_stat(loop, req, path, cb)
                 : uv_fs_fstat(loop, req, fd, cb));
}

static int Lrename(lua_State *L) {
    PATH(1,path); PATH(2,new_path);
    FS_CALL(3, rename, NULL, path, new_path);
}

static int Lfsync(lua_State *L) {
    FD(1,fd);
    FS_CALL(2, fsync, NULL, fd);
}

static int Lfdatasync(lua_State *L) {
    FD(1,fd);
    FS_CALL(2, fdatasync, NULL, fd);
}

static int Lftruncate(lua_State *L) {
    FD(1,fd); OFFSET(2);
    FS_CALL(3, ftruncate, NULL, fd, offset);
}

static int Lsendfile(lua_State *L) {
    FD(1,out_fd); FD(2,in_fd); SIZE(3,length); OFFSET(4);
    FS_CALL(5, sendfile, NULL, out_fd, in_fd, offset, length);
}

static int Lchmod(lua_State *L) {
    PATH_OR_FD(1); MODE(2);
    FS_CALL2(3, chmod, NULL, mode);
}

static int Lutime(lua_State *L) {
    PATH_OR_FD(1); TIME(2,atime); TIME(3,mtime);
    /* TODO using current time */
    FS_CALL2(3, utime, NULL, atime, mtime);
}

static int Llstat(lua_State *L) {
    PATH(1,path);
    FS_CALL(2, lstat, NULL, path);
}

static int Llink(lua_State *L) {
    PATH(1,path); PATH(2,new_path);
    FS_CALL(3, link, NULL, path, new_path);
}

static int Lsymlink(lua_State *L) {
    PATH(1,path); PATH(2,new_path); FLAGS(3);
    FS_CALL(4, symlink, NULL, path, new_path, flags);
}

static int Lreadlink(lua_State *L) {
    PATH(1,path);
    FS_CALL(3, readlink, NULL, path);
}

static int Lchown(lua_State *L) {
    PATH_OR_FD(1); ID(2,uid); ID(3,gid);
    FS_CALL2(4, chown, NULL, uid, gid);
}

LUALIB_API int luaopen_uv_fs(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(open),
        ENTRY(close),
        ENTRY(read),
        ENTRY(unlink),
        ENTRY(write),
        ENTRY(mkdir),
        ENTRY(rmdir),
        ENTRY(readdir),
        ENTRY(stat),
        ENTRY(rename),
        ENTRY(fsync),
        ENTRY(fdatasync),
        ENTRY(ftruncate),
        ENTRY(sendfile),
        ENTRY(chmod),
        ENTRY(utime),
        ENTRY(lstat),
        ENTRY(link),
        ENTRY(symlink),
        ENTRY(readlink),
        ENTRY(chown),
#undef  ENTRY
        { NULL, NULL }
    };
    luaL_newlib(L, libs);
    return 1;
}
