#define LUA_LIB
#include "luv.h"

typedef struct luv_Idle {
    uv_idle_t handle;
    luv_Queue head;
} luv_Idle;

static int Lidle(lua_State *L) {
    luv_Idle *t = (luv_Idle*)lua_newuserdata(L, sizeof(luv_Idle));
    luv_setmetatable(L, LUV_IDLE);
    uv_idle_init(luv_default_loop(L), &t->handle);
    luv_queue_init(&t->head);
    return 1;
}

static void idle_cb(uv_idle_t* handle, int status) {
    luv_Idle *t = container_of(handle, luv_Idle, handle);
    luv_Queue *q;
    ngx_queue_foreach (q, &t->head) {
        luv_Fiber *f = ngx_queue_data(q, luv_Fiber, head);
        lua_settop(f->L, 0);
        lua_pushinteger(f->L, status);
    }
    luv_queue_wakeall(&t->head, NULL);
}

static int Lstart(lua_State *L) {
    luv_Idle *t = luv_check(L, 1, LUV_IDLE);
    luv_call(L, idle_start, (&t->handle, idle_cb));
    lua_settop(L, 1);
    return 1;
}

static int Lstop(lua_State *L) {
    luv_Idle *t = luv_check(L, 1, LUV_IDLE);
    luv_call(L, idle_stop, (&t->handle));
    lua_settop(L, 1);
    return 1;
}

static int Lwait(lua_State *L) {
    luv_Idle *t = luv_check(L, 1, LUV_IDLE);
    luv_Fiber *f = luv_fiber_default(L, 2, NULL);
    return luv_fiber_yield(f, &t->head, 0);
}

LUALIB_API int luaopen_uv_idle(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(start),
        ENTRY(stop),
        ENTRY(wait),
#undef  ENTRY
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_IDLE, libs);
    lua_pushcfunction(L, Lidle);
    return 1;
}
