#define LUA_LIB
#include "luv.h"

#include <assert.h>

int luv_sockaddr_info(lua_State *L, struct sockaddr_storage *addr) {
    char ip[INET6_ADDRSTRLEN];
    struct sockaddr_in  *addrin;
    struct sockaddr_in6 *addrin6;

    switch (addr->ss_family) {
    case AF_INET:
        addrin = (struct sockaddr_in*)addr;
        uv_inet_ntop(AF_INET, &(addrin->sin_addr), ip, INET6_ADDRSTRLEN);
        lua_pushstring(L, "inet");
        lua_pushstring(L, ip);
        lua_pushinteger(L, ntohs(addrin->sin_port));
        return 3;
    case AF_INET6:
        addrin6 = (struct sockaddr_in6*)addr;
        uv_inet_ntop(AF_INET6, &(addrin6->sin6_addr), ip, INET6_ADDRSTRLEN);
        lua_pushstring(L, "inet6");
        lua_pushstring(L, ip);
        lua_pushinteger(L, ntohs(addrin6->sin6_port));
        return 3;
    }

    return 0;
}

void luv_connect_cb(uv_connect_t* req, int status) {
    luv_Fiber *f = container_of(req, luv_Fiber, r);
    lua_settop(f->L, 0);
    if (status < 0)
        luv_push_error(f->L, "connect", status);
    else
        lua_pushinteger(f->L, status);
    luv_fiber_resume(f, NULL);
}


/* tcp interface */

static int Ltcp_bind(lua_State* L) {
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    const char* host = luaL_checkstring(L, 2);
    int port = luaL_checkint(L, 3);
    struct sockaddr_in addr;
    luv_call(L, ip4_addr, (host, port, &addr));
    luv_call(L, tcp_bind, (self, (struct sockaddr*)&addr));
    lua_settop(L, 1);
    return 1;
}

static int Ltcp(lua_State* L) {
  luv_Fiber* f = luv_fiber_current(L);
  luv_Stream *s = luv_stream_new(L);
  luv_setmetatable(L, LUV_TCP);
  uv_tcp_init(f->S->loop, luv_tcp(s));
  if (lua_gettop(L) > 1)
      return Ltcp_bind(L);
  return 1;
}

LUV_IMPLEMENT_COMMON_CB(connect)

static int Ltcp_connect(lua_State* L) {
    luv_Fiber *f = luv_fiber_current(L);
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    const char* host = luaL_checkstring(L, 2);
    int port = luaL_checkint(L, 3);
    struct sockaddr_in addr;
    luv_call(L, ip4_addr, (host, port, &addr));
    luv_call(L, tcp_connect,
            (&f->r.connect, self, (struct sockaddr*)&addr, connect_cb));

    return luv_fiber_yield(f, &f->S->waiting, 0);
}

static int Ltcp_getsockname(lua_State* L) {
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    struct sockaddr_storage addr;
    int len = sizeof(addr);
    luv_call(L, tcp_getsockname, (self, (struct sockaddr*)&addr, &len));
    return luv_sockaddr_info(L, &addr);
}

static int Ltcp_getpeername(lua_State* L) {
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    struct sockaddr_storage addr;
    int len = sizeof(addr);
    luv_call(L, tcp_getpeername, (self, (struct sockaddr*)&addr, &len));
    return luv_sockaddr_info(L, &addr);
}

static int Ltcp_keepalive(lua_State* L) {
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    int enable = 0;
    unsigned int delay = 0;

    if (!lua_isnoneornil(L, 2)) {
        enable = 1;
        delay = luaL_checkint(L, 2);
    }

    luv_call(L, tcp_keepalive, (self, enable, delay));

    lua_settop(L, 1);
    return 1;
}

static int Ltcp_nodelay(lua_State* L) {
    uv_tcp_t *self = (uv_tcp_t*)luv_check(L, 1, LUV_TCP);
    luv_call(L, tcp_nodelay, (self, lua_toboolean(L, 2)));
    lua_settop(L, 1);
    return 1;
}

LUALIB_API int luaopen_uv_net_tcp(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, Ltcp_##name }
        ENTRY(bind),
        ENTRY(connect),
        ENTRY(getsockname),
        ENTRY(getpeername),
        ENTRY(keepalive),
        ENTRY(nodelay),
#undef  ENTRY
        { NULL, NULL },
    };
    luv_newmetatable(L, LUV_TCP, libs);
    luv_stream_openlibs(L);
    lua_pushcfunction(L, Ltcp);
    return 1;
}


/* udp interface */

typedef struct luv_udp {
    uv_udp_t handle;
    luv_Queue recving;
    luv_Queue closing;
} luv_udp;

LUV_IMPLEMENT_COMMON_CB(udp_send)

static void udp_alloc_cb(uv_handle_t *handle, size_t size, uv_buf_t *buf) {
    (void)handle;
    buf->base = (char*)malloc(size);
    buf->len = size;
}

static void udp_recv_cb(uv_udp_t* handle, ssize_t nread, const uv_buf_t *buf,
        const struct sockaddr* addr, unsigned flags) {
    luv_udp *self = (luv_udp*)handle;

    if (!luv_queue_empty(&self->recving)) {
        luv_Fiber *f = luv_queue_head(&self->recving);
        lua_settop(f->L, 0);
        if (nread <= 0) {
            luv_push_error(f->L, "recv", nread);
        }
        else {
            luv_sockaddr_info(f->L, (struct sockaddr_storage*)addr);
            lua_pushlstring(f->L, buf->base, nread);
            lua_replace(f->L, -4); /* replace addr family */
        }
        luv_fiber_resume(f, NULL);
    }

    free(buf->base);
}

static void udp_close_cb(uv_handle_t* handle) {
    luv_udp *self = (luv_udp*)handle;
    luv_Queue *q;
    while (!luv_queue_empty(&self->closing)) {
        luv_Fiber *f = luv_queue_head(&self->closing);
        lua_settop(f->L, 0);
        luv_fiber_resume(f, NULL);
        assert(f->waiting != &self->closing);
    }
    ngx_queue_foreach (q, &self->recving) {
        luv_Fiber *f = ngx_queue_data(q, luv_Fiber, head);
        lua_pushnil(f->L);
        lua_pushstring(f->L, "udp closed");
    }
    luv_queue_wakeall(&self->recving, NULL);
}

static int Ludp(lua_State* L) {
  luv_udp* self = (luv_udp*)lua_newuserdata(L, sizeof(luv_udp));
  luv_setmetatable(L, LUV_UDP);
  uv_udp_init(luv_default_loop(L), &self->handle);
  luv_queue_init(&self->recving);
  luv_queue_init(&self->closing);
  return 1;
}

static int Ludp_close(lua_State *L) {
    luv_Fiber *f = luv_fiber_current(L);
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    uv_close((uv_handle_t*)self, udp_close_cb);
    return luv_fiber_yield(f, &self->recving, 0);
}

static int Ludp_bind(lua_State *L) {
  uv_udp_t* self = (uv_udp_t*)luv_check(L, 1, LUV_UDP);
  const char* host = luaL_checkstring(L, 2);
  int port = luaL_checkint(L, 3);
  struct sockaddr_in addr;
  luv_call(L, ip4_addr, (host, port, &addr));
  luv_call(L, udp_bind, (self, (struct sockaddr*)&addr, 0));
  lua_settop(L, 1);
  return 1;
}

static int Ludp_send(lua_State *L) {
    luv_Fiber *f = luv_fiber_current(L);
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    size_t len;
    const char *host = luaL_checkstring(L, 2);
    int         port = luaL_checkint(L, 3);
    const char *s = luaL_checklstring(L, 4, &len);

    uv_buf_t buf = uv_buf_init((char*)s, len);
    struct sockaddr_in addr;
    luv_call(L, ip4_addr, (host, port, &addr));
    luv_call(L, udp_send,
            (&f->r.udp_send, (uv_udp_t*)self, &buf, 1,
                (struct sockaddr*)&addr, udp_send_cb));
    return luv_fiber_yield(f, &f->S->waiting, 0);
}

static int Ludp_recv(lua_State *L) {
    luv_Fiber *f = luv_fiber_current(L);
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    luv_call(L, udp_recv_start,
            (&self->handle, udp_alloc_cb, udp_recv_cb));
    return luv_fiber_yield(f, &self->recving, 0);
}

static int Ludp_start(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    luv_call(L, udp_recv_start,
            (&self->handle, udp_alloc_cb, udp_recv_cb));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_stop(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    luv_call(L, udp_recv_stop, (&self->handle));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_membership(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    static const char *opts[] = { "join", "leave", NULL };
    const char *iaddr = luaL_checkstring(L, 3);
    const char *maddr = luaL_checkstring(L, 2);

    int option = luaL_checkoption(L, 4, NULL, opts);
    uv_membership membership = option ? UV_LEAVE_GROUP : UV_JOIN_GROUP;
    luv_call(L, udp_set_membership,
            ((uv_udp_t*)self, maddr, iaddr, membership));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_getsockname(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    struct sockaddr_storage addr;
    int len = sizeof(addr);
    luv_call(L, udp_getsockname,
            ((uv_udp_t*)self, (struct sockaddr*)&addr, &len));
    return luv_sockaddr_info(L, &addr);
}

static int Ludp_set_multicast_loop(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    int on = lua_toboolean(L, 2);
    luv_call(L, udp_set_multicast_loop, ((uv_udp_t*)self, on));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_set_multicast_ttl(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    int ttl = luaL_checkint(L, 2);
    luv_call(L, udp_set_multicast_ttl, ((uv_udp_t*)self, ttl));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_set_broadcast(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    int on = lua_toboolean(L, 2);
    luv_call(L, udp_set_broadcast, ((uv_udp_t*)self, on));
    lua_settop(L, 1);
    return 1;
}

static int Ludp_setttl(lua_State *L) {
    luv_udp* self = (luv_udp*)luv_check(L, 1, LUV_UDP);
    int ttl = luaL_checkint(L, 2);
    luv_call(L, udp_set_ttl, ((uv_udp_t*)self, ttl));
    lua_settop(L, 1);
    return 1;
}

LUALIB_API int luaopen_uv_net_udp(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, Ludp_##name }
        ENTRY(close),
        ENTRY(bind),
        ENTRY(send),
        ENTRY(recv),
        ENTRY(start),
        ENTRY(stop),
        ENTRY(membership),
        ENTRY(getsockname),
        ENTRY(set_multicast_loop),
        ENTRY(set_multicast_ttl),
        ENTRY(set_broadcast),
        ENTRY(setttl),
#undef  ENTRY
        { "__gc", Ludp_close },
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_UDP, libs);
    lua_pushcfunction(L, Ludp);
    return 1;
}
