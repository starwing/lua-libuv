#define LUA_LIB
#include "luv.h"

static int Lpipe(lua_State *L) {
    luv_Stream *s = luv_stream_new(L);
    luv_setmetatable(L, LUV_NAMED_PIPE);
    int ipc = lua_toboolean(L, 1);
    uv_pipe_init(luv_default_loop(L), luv_pipe(s), ipc);
    return 1;
}

static int Lopen(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    int fh = luv_check_file(L, 2);
    luv_call(L, pipe_open, (luv_pipe(s), fh));
    lua_settop(L, 1);
    return 1;
}

static int Lbind(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    const char *path = luaL_checkstring(L, 2);
    luv_call(L, pipe_bind, (luv_pipe(s), path));
    lua_settop(L, 1);
    return 1;
}

LUV_IMPLEMENT_COMMON_CB(connect)

static int Lconnect(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    const char *path = luaL_checkstring(L, 2);
    luv_Fiber *f = luv_fiber_current(L);
    uv_pipe_connect(&f->r.connect, luv_pipe(s), path, connect_cb);
    return luv_common_yield(f);
}

LUALIB_API int luaopen_uv_pipe(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(open),
        ENTRY(bind),
        ENTRY(connect),
#undef  ENTRY
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_NAMED_PIPE, libs);
    luv_stream_openlibs(L);
    lua_pushcfunction(L, Lpipe);
    return 1;
}
