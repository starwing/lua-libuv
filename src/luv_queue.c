#define LUA_LIB
#include "luv.h"

#include <assert.h>

luv_Queue *luv_queue_new(lua_State *L) {
    luv_Queue *q = (luv_Queue*)lua_newuserdata(L, sizeof(luv_Queue));
    luv_setmetatable(L, LUV_QUEUE);
    ngx_queue_init(q);
    return q;
}

void luv_queue_del(luv_Queue *q) {
    luv_Queue *errors = NULL;
    while (!luv_queue_empty(q)) {
        luv_Fiber *f = luv_queue_head(q);
        assert(f->L != NULL);
        if (errors == NULL)
            errors = luv_state_errors(f->L);
        lua_settop(f->L, 0);
        lua_pushnil(f->L);
        lua_pushstring(f->L, "wait queue deleted");
        luv_fiber_resume(f, NULL);
        if (f->waiting == q) {
            lua_pushnil(f->L);
            lua_pushstring(f->L, "attempt to wait a finished queue");
            f->waiting = NULL;
            luv_queue_append(errors, &f->head);
        }
    }
}

int luv_queue_pusherrors(lua_State *L, const char *method, luv_Queue *error) {
    luaL_Buffer b;
    lua_pushnil(L);
    luaL_buffinit(L, &b);
    lua_pushfstring(L, "errors in %s:\n", method);
    luaL_addvalue(&b);
    while (!luv_queue_empty(error)) {
        luv_Fiber *f = luv_queue_head(error);
        luaL_addstring(&b, "----------\n");
        luaL_traceback(L, f->L, lua_tostring(f->L, -1), 0);
        luaL_addvalue(&b);
        luv_fiber_del(f);
    }
    luaL_addstring(&b, "\n----------\n");
    luaL_pushresult(&b);
    return 2;
}

int luv_queue_wait(luv_Queue *q, lua_State *L, int nargs) {
    luv_Fiber *f = luv_fiber_current(L);
    return luv_fiber_yield(f, q, nargs);
}

void luv_queue_wakeall(luv_Queue *q, lua_State *L) {
    luv_Queue again;
    ngx_queue_init(&again);
    while (!ngx_queue_empty(q)) {
        luv_Fiber *f = luv_queue_head(q);
        luv_fiber_resume(f, L);
        if (f->waiting == q)
            luv_queue_append(&again, &f->head);
    }
    ngx_queue_add(q, &again);
}

static int Lcreate(lua_State *L) {
    luv_queue_new(L);
    return 1;
}

static int Lwait(lua_State *L) {
    luv_Queue *q = (luv_Queue*)luv_check(L, 1, LUV_QUEUE);
    return luv_queue_wait(q, L, lua_gettop(L)-1);
}

static int Lwakeup(lua_State *L) {
    luv_Queue *q = (luv_Queue*)luv_check(L, 1, LUV_QUEUE);
    luv_Fiber *f = luv_queue_head(q);
    lua_settop(f->L, 0);
    lua_xmove(L, f->L, lua_gettop(L)-1);
    if (luv_fiber_resume(f, L) != LUA_OK) {
        lua_pushnil(L);
        lua_xmove(f->L, L, 1);
        luv_fiber_del(f);
        return 2;
    }
    lua_settop(L, 0);
    lua_pushboolean(L, 1);
    lua_xmove(f->L, L, lua_gettop(f->L));
    return lua_gettop(L);
}

static int Lwakeall(lua_State *L) {
    luv_Queue *q = (luv_Queue*)luv_check(L, 1, LUV_QUEUE);
    int top = lua_gettop(L);
    luv_Queue again, error;
    ngx_queue_init(&again);
    ngx_queue_init(&error);
    while (!luv_queue_empty(q)) {
        luv_Fiber *f = luv_queue_head(q);
        lua_settop(f->L, 0);
        luv_copystack(L, f->L, top-1);
        if (luv_fiber_resume(f, L) != LUA_OK)
            luv_queue_append(&error, &f->head);
        else if (f->waiting == q)
            luv_queue_append(&again, &f->head);
    }
    ngx_queue_add(q, &again);
    if (ngx_queue_empty(&error)) {
        lua_settop(L, 1);
        return 1;
    }
    return luv_queue_pusherrors(L, "wakeall", &error);
}

static int L__gc(lua_State *L) {
    luv_Queue *q = (luv_Queue*)luv_check(L, 1, LUV_QUEUE);
    luv_queue_del(q);
    return 0;
}

LUALIB_API int luaopen_uv_queue (lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(wait),
        ENTRY(wakeup),
        ENTRY(wakeall),
        ENTRY(__gc),
#undef  ENTRY
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_QUEUE, libs);
    lua_pushcfunction(L, Lcreate);
    return 1;
}
