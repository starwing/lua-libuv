#define LUA_LIB
#include "luv.h"

#include <assert.h>

#define LUV_MAIN_STATE ((void*)0x57A7EB03)

static void async_cb(uv_async_t* handle, int status) {
    /* interrupt loop */
    (void)status;
    uv_unref((uv_handle_t*)handle);
}

luv_State *luv_main_state(lua_State *L) {
    luv_State *S;
    lua_rawgetp(L, LUA_REGISTRYINDEX, LUV_MAIN_STATE);
    S = (luv_State*)lua_touserdata(L, -1);
    lua_pop(L, 1);
    if (S == NULL) {
        S = (luv_State*)lua_newuserdata(L, sizeof(luv_State));
        S->L = luv_main_luastate(L);
        S->loop = uv_default_loop();
        uv_async_init(S->loop, &S->async, async_cb);
        uv_unref((uv_handle_t*)&S->async);
        S->is_ready = 0;
        ngx_queue_init(&S->running);
        ngx_queue_init(&S->waiting);
        ngx_queue_init(&S->errors);
        lua_rawsetp(L, LUA_REGISTRYINDEX, LUV_MAIN_STATE);
    }
    return S;
}

lua_State *luv_main_luastate(lua_State *L) {
    lua_State *mainL;
    lua_rawgeti(L, LUA_REGISTRYINDEX, LUA_RIDX_MAINTHREAD);
    mainL = lua_tothread(L, -1);
    lua_pop(L, 1);
    return mainL;
}

int luv_state_ready(luv_State *S) {
    if (!S->is_ready) {
        S->is_ready = 1;
        /* interrupt the event loop (the sequence of these two calls matters) */
        uv_async_send(&S->async);
        /* make sure we loop at least once more */
        uv_ref((uv_handle_t*)&S->async);
    }
    return 0;
}

int luv_state_join(luv_State *S) {
    lua_State *L = S->L;
    int r = 0, has_errors = 0;

    S->is_ready = 0;
    do {
        /* work out pending fibers */
        luv_queue_wakeall(&S->running, L);
        /* do event loop */
        r = uv_run(S->loop, UV_RUN_ONCE);
        if (!luv_queue_empty(&S->errors))
            has_errors = 1;
        S->is_ready = 0;
    } while (r && !has_errors);

    if (!has_errors) {
        lua_pushinteger(L, r);
        return 1;
    }

    return luv_queue_pusherrors(L, "join", &S->errors);
}
