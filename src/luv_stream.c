#define LUA_LIB
#include "luv.h"


#include <assert.h>

#define stream_is_closing(s) uv_is_closing((uv_handle_t*)(s))
#define stream_is_valid(s)  (luv_stream(s)->data != NULL)

/* stream interface */

static void alloc_cb(uv_handle_t* handle, size_t size, uv_buf_t *buf);
static void read_cb(uv_stream_t* stream, ssize_t nread, const uv_buf_t *buf);
static void close_cb(uv_handle_t* handle);

static void buf_new(uv_buf_t *buf, size_t size) {
    buf->base = (char*)malloc(size);
    buf->len = size;
}

static void buf_del(const uv_buf_t *buf) {
    free(buf->base);
}

luv_Stream *luv_stream_new(lua_State *L) {
    luv_Stream *s = (luv_Stream*)lua_newuserdata(L, sizeof(luv_Stream));
    luv_stream(s)->data = L; /* non-NULL if stream valid */
    s->count = 0;
    s->buf = uv_buf_init(NULL, 0);
    ngx_queue_init(&s->q.listening);
    ngx_queue_init(&s->closing);
    return s;
}

void luv_stream_del(luv_Stream *s) {
    if (!stream_is_valid(s)) return;
    buf_del(&s->buf);
    if (!stream_is_closing(s)) {
        uv_close((uv_handle_t*)s, close_cb);
    }
}

luv_Stream *luv_stream_test(lua_State *L, int narg) {
    switch (luv_type(L, narg)) {
#define XX(e,t,n) case LUV_##e:
    LUV_STREAM_TYPES(XX)
#undef  XX
        return (luv_Stream*)lua_touserdata(L, narg);
    default:
        return NULL;
    }
}

luv_Stream *luv_stream_check(lua_State *L, int narg) {
    luv_Stream *s = luv_stream_test(L, narg);
    if (s == NULL) luv_typeerror(L, narg, "stream");
    return s;
}

int luv_stream_start(luv_Stream *s) {
    if (uv_is_readable(luv_stream(s))) {
        int res = uv_read_start(luv_stream(s), alloc_cb, read_cb);
        if (res == UV_EALREADY)
            return 0;
        return res;
    }
    return 0;
}

int luv_stream_stop(luv_Stream *s) {
    return uv_read_stop(luv_stream(s));
}


/* callbacks */

LUV_IMPLEMENT_COMMON_CB(write)
LUV_IMPLEMENT_COMMON_CB(shutdown)

static void connection_cb(uv_stream_t* server, int status) {
    luv_Stream *s = (luv_Stream*)server;

    while (!ngx_queue_empty(&s->q.listening)) {
        int err;
        luv_Fiber *f = luv_queue_head(&s->q.listening);
        luv_Stream *c = luv_stream_test(f->L, 2);
        if (c == NULL) {
            lua_settop(f->L, 0);
            lua_pushnil(f->L);
            luv_typeerror(f->L, 2, "stream");
            luv_fiber_resume(f, NULL);
            continue;
        }
        if ((err = uv_accept(luv_stream(s), luv_stream(c))) != 0) {
            lua_settop(f->L, 0);
            luv_push_error(f->L, "accept", err);
            luv_fiber_resume(f, NULL);
            continue;
        }
        lua_pop(f->L, 1);
        luv_fiber_resume(f, NULL);
        return;
    }
    
    ++s->count;
}

static void alloc_cb(uv_handle_t* handle, size_t size, uv_buf_t *buf) {
    luv_Stream *s = (luv_Stream*)handle;
    buf_new(buf, s->count ? s->count : size);
}

static void read_cb(uv_stream_t* stream, ssize_t nread, const uv_buf_t *buf) {
    luv_Stream *s = (luv_Stream*)stream;
    luv_Queue *q;

    ngx_queue_foreach (q, &s->q.reading) {
        luv_Fiber *f = ngx_queue_data(q, luv_Fiber, head);
        assert(s->count == 0);
        lua_settop(f->L, 0);
        if (nread >= 0) {
            lua_pushinteger(f->L, nread);
            lua_pushlstring(f->L, buf->base, nread);
        }
        else if (nread != UV_EOF)
            luv_push_error(f->L, "read", nread);
        buf_del(buf);
        if (nread <= 0) luv_stream_stop(s);
        if (nread < 0) luv_stream_del(s);
        luv_fiber_resume(f, NULL);
        return;
    }

    /* no fiber wait for read :( */
    luv_stream_stop(s);
    if (nread < 0) {
        buf_del(buf);
        return;
    }

    if (s->buf.base != NULL) {
        size_t len = s->buf.len + nread;
        char *p = (char*)realloc(s->buf.base, len);
        if (p == NULL) return; /* XXX how to report this? */
        memcpy(&p[s->buf.len], buf->base, buf->len);
        s->buf = uv_buf_init(p, len);
    }
    else 
        s->buf = *buf;
    s->count = nread;
}

static void close_cb(uv_handle_t* handle) {
    luv_Stream *s = (luv_Stream*)handle;
    luv_Queue *q;
    while (!luv_queue_empty(&s->closing)) {
        luv_Fiber *f = luv_queue_head(&s->closing);
        lua_settop(f->L, 0);
        luv_fiber_resume(f, NULL);
        assert(f->waiting != &s->q.reading &&
               f->waiting != &s->closing);
    }
    ngx_queue_foreach (q, &s->q.reading) {
        luv_Fiber *f = ngx_queue_data(q, luv_Fiber, head);
        lua_settop(f->L, 0);
        lua_pushnil(f->L);
        lua_pushstring(f->L, "stream closed");
        lua_pushstring(f->L, "closed");
    }
    luv_queue_wakeall(&s->q.reading, NULL);
}


/* stream lua interface */

static int Llisten(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    int backlog = luaL_optint(L, 2, 128);
    luv_call(L, listen, (luv_stream(s), backlog, connection_cb));
    lua_settop(L, 1);
    return 1;
}

static int Laccept(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    luv_Stream *c = luv_stream_check(L, 2);

    if (s->count != 0) {
        --s->count;
        luv_call(L, accept, (luv_stream(s), luv_stream(c)));
        lua_settop(L, 1);
        return 1;
    }

    lua_settop(L, 2);
    return luv_queue_wait(&s->q.listening, L, 2);
}

static int Lstart(lua_State *L) {
    int err;
    luv_Stream *s = luv_stream_check(L, 1);

    if ((err = luv_stream_start(s)) != 0)
        return luv_push_error(L, "start", err);

    lua_settop(L, 1);
    return 1;
}

static int Lstop(lua_State *L) {
    int err;
    luv_Stream *s = luv_stream_check(L, 1);

    if ((err = luv_stream_stop(s)) != 0)
        return luv_push_error(L, "stop", err);

    lua_settop(L, 1);
    return 1;
}

static int Lread(lua_State *L) {
    int err;
    luv_Stream *s = luv_stream_check(L, 1);
    luv_Fiber *f = luv_fiber_current(L);
    int len = luaL_optint(L, 2, 0);

    if (s->buf.base != NULL) { /* has a buffer? */
        lua_pushinteger(L, s->count);
        lua_pushlstring(L, s->buf.base, s->buf.len);
        buf_del(&s->buf);
        s->count = 0;
        return 2;
    }

    s->count = len;
    if ((err = luv_stream_start(s)) != 0)
        return luv_push_error(L, "read", err);

    return luv_fiber_yield(f, &s->q.reading, 0);
}

static int Lwrite(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    luv_Fiber *f = luv_fiber_current(L);
    size_t len;
    const char *chunk = luaL_checklstring(L, 2, &len);
    uv_buf_t buf = uv_buf_init((char*)chunk, len);
    luv_call(L, write, (&f->r.write, luv_stream(s), &buf, 1, write_cb));
    return luv_fiber_yield(f, &f->S->waiting, 0);
}

static int Lreadable(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    lua_pushboolean(L, uv_is_readable(luv_stream(s)));
    return 1;
}

static int Lwriteable(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    lua_pushboolean(L, uv_is_writable(luv_stream(s)));
    return 1;
}

static int Lshutdown(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    luv_Fiber *f = luv_fiber_current(L);
    luv_call(L, shutdown, (&f->r.shutdown, luv_stream(s), shutdown_cb));
    return luv_fiber_yield(f, &f->S->waiting, 0);
}

static int Lclosing(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    lua_settop(L, 1);
    return uv_is_closing((uv_handle_t*)s);
}

static int Lclose(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    luv_Fiber *f = luv_fiber_current(L);
    if (!stream_is_closing(s)) {
        luv_stream_del(s);
    }
    return luv_fiber_yield(f, &s->closing, 0);
}

static int L__gc(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
        luv_stream_del(s);
    return 0;
}

void luv_stream_openlibs(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(listen),
        ENTRY(accept),
        ENTRY(start),
        ENTRY(stop),
        ENTRY(read),
        ENTRY(write),
        ENTRY(readable),
        ENTRY(writeable),
        ENTRY(shutdown),
        ENTRY(closing),
        ENTRY(close),
        ENTRY(__gc),
#undef  ENTRY
        { NULL, NULL }
    };
    luaL_setfuncs(L, libs, 0);
}
