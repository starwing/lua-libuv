#define LUA_LIB
#include "luv.h"

typedef struct luv_Timer {
    uv_timer_t handle;
    luv_Queue head;
} luv_Timer;

static int Ltimer(lua_State *L) {
    luv_Timer *t = (luv_Timer*)lua_newuserdata(L, sizeof(luv_Timer));
    luv_setmetatable(L, LUV_TIMER);
    uv_timer_init(luv_default_loop(L), &t->handle);
    luv_queue_init(&t->head);
    return 1;
}

static void timer_cb(uv_timer_t* handle, int status) {
    luv_Timer *t = container_of(handle, luv_Timer, handle);
    luv_Queue *q;
    ngx_queue_foreach (q, &t->head) {
        luv_Fiber *f = ngx_queue_data(q, luv_Fiber, head);
        lua_settop(f->L, 0);
        lua_pushinteger(f->L, status);
    }
    luv_queue_wakeall(&t->head, NULL);
}

static int Lstart(lua_State *L) {
    luv_Timer *t = luv_check(L, 1, LUV_TIMER);
    int repeat  = luaL_checkint(L, 2);
    int timeout = luaL_optint(L, 3, 0);
    luv_call(L, timer_start, (&t->handle, timer_cb, timeout, repeat));
    lua_settop(L, 1);
    return 1;
}

static int Lstop(lua_State *L) {
    luv_Timer *t = luv_check(L, 1, LUV_TIMER);
    luv_call(L, timer_stop, (&t->handle));
    lua_settop(L, 1);
    return 1;
}

static int Lagain(lua_State *L) {
    luv_Timer *t = luv_check(L, 1, LUV_TIMER);
    luv_call(L, timer_again, (&t->handle));
    lua_settop(L, 1);
    return 1;
}

static int Lrepeat(lua_State *L) {
    int repeat;
    luv_Timer *t = luv_check(L, 1, LUV_TIMER);

    if (lua_gettop(L) == 0)
        lua_pushinteger(L, uv_timer_get_repeat(&t->handle));
    else {
        repeat = luaL_checkint(L, 1);
        uv_timer_set_repeat(&t->handle, repeat);
        lua_settop(L, 1);
    }

    return 1;
}

static int Lwait(lua_State *L) {
    luv_Timer *t = luv_check(L, 1, LUV_TIMER);
    luv_Fiber *f = luv_fiber_default(L, 2, NULL);
    return luv_fiber_yield(f, &t->head, 0);
}

LUALIB_API int luaopen_uv_timer(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(start),
        ENTRY(stop),
        ENTRY(again),
        ENTRY(repeat),
        ENTRY(wait),
#undef  ENTRY
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_TIMER, libs);
    lua_pushcfunction(L, Ltimer);
    return 1;
}
