#define LUA_LIB
#include "luv.h"

static int Ltty(lua_State *L) {
    int fh = luv_check_file(L, 1);
    int readable = lua_toboolean(L, 2);
    luv_Stream *s = luv_stream_new(L);
    luv_setmetatable(L, LUV_TTY);
    uv_tty_init(luv_default_loop(L), luv_tty(s), fh, readable);
    return 1;
}

static int Lsetmode(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    static const char *opts[] = { "normal", "raw" };
    int option = luaL_checkoption(L, 2, NULL, opts);
    luv_call(L, tty_set_mode, (luv_tty(s), option));
    lua_settop(L, 1);
    return 1;
}

static int Lresetmode(lua_State *L) {
    uv_tty_reset_mode();
    return 0;
}

static int Lwinsize(lua_State *L) {
    luv_Stream *s = luv_stream_check(L, 1);
    int width, height;
    luv_call(L, tty_get_winsize, (luv_tty(s), &width, &height));
    lua_pushinteger(L, width);
    lua_pushinteger(L, height);
    return 2;
}


LUALIB_API int luaopen_uv_tty(lua_State *L) {
    luaL_Reg libs[] = {
#define ENTRY(name) { #name, L##name }
        ENTRY(setmode),
        ENTRY(resetmode),
        ENTRY(winsize),
#undef  ENTRY
        { NULL, NULL }
    };
    luv_newmetatable(L, LUV_TTY, libs);
    luv_stream_openlibs(L);
    lua_pushcfunction(L, Ltty);
    return 1;
}
