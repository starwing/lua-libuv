#define LUA_LIB
#include "luv.h"

const char *luv_typename (luv_Type t) {
    switch (t) {
#define XX(e,t,n) case LUV_##e: return n;
        LUV_UV_TYPES(XX)
        LUV_TYPES(XX)
#undef  XX
    default:
        return "uv.unknown";
    }
}

luv_Type luv_type (lua_State *L, int narg) {
    luv_Type t;
    if (lua_getmetatable(L, narg)) {
        lua_rawgeti(L, -1, 0);
        t = lua_tonumber(L, -1);
        lua_pop(L, 2);
        return t;
    }
    return LUV_TYPE_UNKNOWN;
}

void *luv_test(lua_State *L, int narg, luv_Type t) {
    return luv_type(L, narg) == t ? lua_touserdata(L, narg) : NULL;
}

void *luv_check(lua_State *L, int narg, luv_Type t) {
    void *obj = luv_test(L, narg, t);
    if (obj == NULL)
        luv_typeerror(L, narg, luv_typename(t));
    return obj;
}

int luv_check_file(lua_State *L, int narg) {
    uv_file *f = (uv_file*)luv_test(L, 2, LUV_FILE);
    return f == NULL ? luaL_checkint(L, 2) : *f;
}

int luv_argferror(lua_State *L, int narg, const char *fmt, ...) {
    const char *errmsg;
    va_list argp;
    va_start(argp, fmt);
    errmsg = lua_pushvfstring(L, fmt, argp);
    va_end(argp);
    return luaL_argerror(L, narg, errmsg);
}

int luv_typeerror(lua_State *L, int narg, const char *tname) {
    luv_Type t = luv_type(L, narg);
    const char *errmsg = lua_pushfstring(L,
            "%s expected, got %s", tname,
            t != 0 ? luv_typename(t)
                   : luaL_typename(L, narg));
    return luaL_argerror(L, narg, errmsg);
}

int luv_tostring(lua_State *L) {
    luv_Type t = luv_type(L, 1);
    if (t != 0)
        lua_pushfstring(L, "%s: %p",
                luv_typename(t),
                lua_topointer(L, 1));
    else
        luaL_tolstring(L, 1, NULL);
    return 1;
}

int luv_newindex(lua_State *L) {
    luaL_checktype(L, 1, LUA_TUSERDATA);
    lua_getuservalue(L, 1);
    if (lua_isnil(L, -1)) {
        lua_pop(L, 1);
        lua_createtable(L, 0, 1);
        lua_pushvalue(L, -1);
        lua_setuservalue(L, 1);
    }
    lua_pushvalue(L, 2);
    lua_pushvalue(L, 3);
    lua_rawset(L, -3);
    return 0;
}

int luv_index(lua_State *L) {
    luaL_checktype(L, 1, LUA_TUSERDATA);
    lua_getuservalue(L, 1);
    if (!lua_isnil(L, -1)) {
        lua_pushvalue(L, 2);
        lua_rawget(L, -2);
        if (!lua_isnil(L, -1))
            return 1;
    }
    lua_getmetatable(L, 1);
    lua_pushvalue(L, 2);
    lua_rawget(L, -2);
    return 1;
}

int luv_copystack(lua_State *from, lua_State *to, int nargs) {
    int i;
    luaL_checkstack(from, nargs, "too many args");
    for (i = 0; i < nargs; ++i)
        lua_pushvalue(from, -nargs);
    lua_xmove(from, to, nargs);
    return nargs;
}

int luv_dumpstack(lua_State *L) {
    int i, top;
    printf("-------------------\n");
    printf("dumpstack: L=%p\n", L);
    top = lua_gettop(L);
    printf("top: %d\n", top);
    for (i = 1; i <= top; ++i) {
        printf("[%d][%s]: %s\n",
                i,
                luaL_typename(L, i),
                luaL_tolstring(L, i, NULL));
        lua_pop(L, 1);
    }
    printf("-------------------\n");
    return top;
}

void luv_newmetatable(lua_State *L, luv_Type t, luaL_Reg *libs) {
    luaL_newlib(L, libs);
    lua_pushinteger(L, t);
    lua_rawseti(L, -2, 0);
    lua_pushvalue(L, -1);
    lua_pushcclosure(L, luv_index, 1);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, luv_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, luv_tostring);
    lua_setfield(L, -2, "__tostring");
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, luv_typename(t));
}

void luv_setmetatable(lua_State *L, luv_Type t) {
    lua_getfield(L, LUA_REGISTRYINDEX, luv_typename(t));
    lua_setmetatable(L, -2);
}
