local fiber = require 'uv.fiber'
local tcp = require 'uv.net.tcp'

fiber(function()
   local client = tcp()
   assert(client:connect("127.0.0.1", 12345))

   while true do
      local s = io.read "*l"
      assert(client:write(s))

      local n, data = client:read()
      if not n then
         if not data then 
            client:close()
            break
         end
         print("error: ", data)
         return
      end
      print("recv: ", data)
   end
end):join()
