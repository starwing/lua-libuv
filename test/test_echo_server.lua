local fiber = require 'uv.fiber'
local tcp = require 'uv.net.tcp'

local server = tcp()
assert(server:bind("0.0.0.0", 12345))
assert(server:listen())
print "listening at 12345"

fiber(function()
   while true do
      local peer = tcp()
      assert(server:accept(peer))

      fiber(function()
         print(tostring(peer), "connected: ", peer:getsockname())
         while true do
            local n, data = peer:read()
            if not n then
               if not data then return peer:close() end
               print("error: ", data)
               break
            end
            print(n)
            print(tostring(peer), "recv: ", data)
            assert(peer:write(data))
         end
      end):ready()
   end
end):join()
